SimpleTodo::Application.routes.draw do
  resources :tasks do
    collection do
      get :remove_done
    end
  end
  authenticated :user do
    root :to => 'tasks#index'
  end
  root :to => "home#index"
  devise_for :users
  resources :users
end