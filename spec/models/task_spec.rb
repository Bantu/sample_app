require 'spec_helper'

describe Task do
  it { should validate_presence_of(:caption) }
  it { should belong_to(:user) }
  
  context 'scopes' do 
    describe 'done' do
      before do
        @done = Task.create(caption: 'done task', done: true)
        @waiting = Task.create(caption: 'waiting task', done: false)
      end
      
      it 'should returns false' do
        Task.done.should eq [@waiting]
      end
    end
  end
end
