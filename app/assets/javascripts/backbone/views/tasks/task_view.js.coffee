class SimpleTodo.Views.Task extends Backbone.View
  template: JST["backbone/templates/tasks/task"]

  events:
    "click .destroy" : "destroy"
    "change input[type=checkbox]" : "toggleDone"

  tagName: "li"
  
  toggleDone: ->
    @model.set('done', !@model.get('done'))
    @model.save()

  destroy: () ->
    @model.destroy()
    this.remove()

    return false
  
  render: ->
    $(@el).html(@template(@model))
    if @model.get('done') == true
      $(@el).addClass('done')
      $(@el).find('input[type=checkbox]').attr('checked', true)
    return this
