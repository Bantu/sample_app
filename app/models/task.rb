class Task < ActiveRecord::Base
  validates_presence_of(:caption)
  attr_accessible :caption, :done
  belongs_to :user
  scope :done, where(done: true)
end
